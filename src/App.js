import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Navbar from "./components/Navbar/Navbar";
import Table from "./components/Table/Table";

import Cat from "./containers/Cat/Cat";
import Dog from "./containers/Dog/Dog";

// import PrivateRoute from "./modules/PrivateRoute";

function App() {

  const cols = [
    {
      label: "First",
      property: "first"
    },
    {
      label: "Last",
      property: "last"
    },
    {
      label: "Handle",
      property: "handle"
    }
  ];

  const rows = [
    {
      first: "Mark",
      last: "Otto",
      handle: "@mda"
    },
    {
      first: "Jacob",
      last: "Thornton",
      handle: "@fat"
    },
    {
      first: "Larry the Bird",
      last: "",
      handle: "@twitter"
    }
  ];

  return (
    <Router>

      <Navbar />

      <div className="container">
        <Switch>
          <Route exact path="/">
            <Table cols={cols} rows={rows} />
          </Route>
          <Route exact path="/cat">
            <Cat />
          </Route>
          <Route exact path="/dog">
            <Dog />
          </Route>
        </Switch>
      </div>

    </Router>
  );
}

export default App;
