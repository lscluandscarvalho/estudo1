import { Route, Redirect } from "react-router-dom";

function PrivateRoute({ children, ...rest }) {

  const auth = true;

  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: location }
            }}
          />
        )
      }
    />
  );

}

export default PrivateRoute;