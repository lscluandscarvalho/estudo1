function Table({ cols, rows }) {

  return (
    <table className="table">
      <thead>
        <tr>
          {
            cols.map(({ label }, index) =>
              <th key={index} scope="col">{label}</th>
            )
          }
        </tr>
      </thead>
      <tbody>
        {
          rows.map((row, index) =>
            <tr key={index}>
              {
                cols.map(({ property }) =>
                  <td key={property}>{row[property]}</td>
                )
              }
            </tr>
          )
        }
      </tbody>
    </table>
  );

}

export default Table;
